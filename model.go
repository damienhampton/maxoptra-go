package maxoptra

type OrdersResponse struct {
	Data   []OrderPartial `json:"data"`
	Offset int64          `json:"offset"`
	Links  Links          `json:"_links"`
}

type OrderPartial struct {
	ReferenceNumber             string                      `json:"referenceNumber"`
	ConsignmentReference        interface{}                 `json:"consignmentReference"`
	DistributionCentreReference DistributionCentreReference `json:"distributionCentreReference"`
	DistributionCentreName      DistributionCentreName      `json:"distributionCentreName"`
	Task                        Task                        `json:"task"`
	Priority                    Priority                    `json:"priority"`
	ClientName                  string                      `json:"clientName"`
	ContactPerson               *string                     `json:"contactPerson"`
	CustomerLocation            CustomerLocation            `json:"customerLocation"`
	Capacity1                   float64                     `json:"capacity1"`
	Capacity2                   float64                     `json:"capacity2"`
	OperationDuration           int64                       `json:"operationDuration"`
	CustomFields                CustomFields                `json:"customFields"`
	Status                      interface{}                 `json:"status"`
}

type CustomerLocation struct {
	ReferenceNumber string      `json:"referenceNumber"`
	Name            string      `json:"name"`
	Address         string      `json:"address"`
	W3WAddress      interface{} `json:"w3wAddress"`
	Latitude        float64     `json:"latitude"`
	Longitude       float64     `json:"longitude"`
}

type Links struct {
	Prev interface{} `json:"prev"`
	Next string      `json:"next"`
}

type DistributionCentreName string
type DistributionCentreReference string
type Priority string
type Task string

const (
	Collection Task = "COLLECTION"
	Delivery   Task = "DELIVERY"
)

type OrderResponse struct {
	Data     Order         `json:"data"`
	Errors   []interface{} `json:"errors"`
	Warnings []interface{} `json:"warnings"`
}

type Order struct {
	ReferenceNumber             string                  `json:"referenceNumber"`
	ConsignmentReference        interface{}             `json:"consignmentReference"`
	DistributionCentreReference string                  `json:"distributionCentreReference"`
	DistributionCentreName      string                  `json:"distributionCentreName"`
	Task                        string                  `json:"task"`
	Priority                    string                  `json:"priority"`
	VehicleRequirements         []string                `json:"vehicleRequirements"`
	AdditionalInstructions      interface{}             `json:"additionalInstructions"`
	ClientName                  string                  `json:"clientName"`
	ContactPerson               string                  `json:"contactPerson"`
	ContactNumber               string                  `json:"contactNumber"`
	ContactEmail                string                  `json:"contactEmail"`
	NotificationPreferences     NotificationPreferences `json:"notificationPreferences"`
	CustomerLocation            CustomerLocation        `json:"customerLocation"`
	TerritoryReference          interface{}             `json:"territoryReference"`
	StopSequence                string                  `json:"stopSequence"`
	TimeWindows                 []TimeWindow            `json:"timeWindows"`
	Capacity1                   float64                 `json:"capacity1"`
	Capacity2                   float64                 `json:"capacity2"`
	OperationDuration           int64                   `json:"operationDuration"`
	CustomFields                CustomFields            `json:"customFields"`
	OrderItems                  []OrderItem             `json:"orderItems"`
}

type CustomFields struct {
}

type NotificationPreferences struct {
	AllowSMS   bool `json:"allowSMS"`
	AllowEmail bool `json:"allowEmail"`
}

type OrderItem struct {
	ItemReferenceNumber  interface{} `json:"itemReferenceNumber"`
	OrderReferenceNumber string      `json:"orderReferenceNumber"`
	Name                 string      `json:"name"`
	Description          interface{} `json:"description"`
	Barcode              string      `json:"barcode"`
	Status               string      `json:"status"`
	RejectReason         interface{} `json:"rejectReason"`
	RejectComment        interface{} `json:"rejectComment"`
	PricePerUnit         float64     `json:"pricePerUnit"`
	PlannedQuantity      int64       `json:"plannedQuantity"`
	FactQuantity         int64       `json:"factQuantity"`
	TotalAmount          float64     `json:"totalAmount"`
}

type TimeWindow struct {
	Start string `json:"start"`
	End   string `json:"end"`
}

type MaxoptraError struct {
	Timestamp string `json:"timestamp"`
	Status    int64  `json:"status"`
	Error     string `json:"error"`
	Path      string `json:"path"`
}

type VehiclesResponse struct {
	Data   []Vehicle `json:"data"`
	Offset int64     `json:"offset"`
	Links  Links     `json:"_links"`
}

type Vehicle struct {
	ReferenceNumber             string                      `json:"referenceNumber"`
	Name                        string                      `json:"name"`
	Capacity1                   float64                     `json:"capacity1"`
	Capacity2                   float64                     `json:"capacity2"`
	DistributionCentreReference DistributionCentreReference `json:"distributionCentreReference"`
	DistributionCentreName      DistributionCentreName      `json:"distributionCentreName"`
	AssignedDriverReference     *string                     `json:"assignedDriverReference"`
	AssignedDriverName          *string                     `json:"assignedDriverName"`
}

type DriversResponse struct {
	Data   []DriverPartial `json:"data"`
	Offset int64           `json:"offset"`
	Links  Links           `json:"_links"`
}

type DriverPartial struct {
	ReferenceNumber             string                      `json:"referenceNumber"`
	Name                        string                      `json:"name"`
	AssignedVehicleReference    *string                     `json:"assignedVehicleReference"`
	AssignedVehicleName         *string                     `json:"assignedVehicleName"`
	DistributionCentreReference DistributionCentreReference `json:"distributionCentreReference"`
	DistributionCentreName      DistributionCentreName      `json:"distributionCentreName"`
}

type DriverResponse struct {
	Data     Driver        `json:"data"`
	Errors   []interface{} `json:"errors"`
	Warnings []interface{} `json:"warnings"`
}

type Driver struct {
	ReferenceNumber              string        `json:"referenceNumber"`
	Name                         string        `json:"name"`
	Comment                      string        `json:"comment"`
	Telephone                    string        `json:"telephone"`
	AssignedVehicleReference     string        `json:"assignedVehicleReference"`
	AssignedVehicleName          string        `json:"assignedVehicleName"`
	CostPerHour                  float64       `json:"costPerHour"`
	DistributionCentreReference  string        `json:"distributionCentreReference"`
	DistributionCentreName       string        `json:"distributionCentreName"`
	Territories                  []interface{} `json:"territories"`
	StartOfDayLocation           string        `json:"startOfDayLocation"`
	StartOfDayAddress            interface{}   `json:"startOfDayAddress"`
	VisitDistributionCentreStart interface{}   `json:"visitDistributionCentreStart"`
	EndOfDayLocation             string        `json:"endOfDayLocation"`
	EndOfDayAddress              interface{}   `json:"endOfDayAddress"`
	VisitDistributionCentreEnd   interface{}   `json:"visitDistributionCentreEnd"`
	DrivingLimit                 interface{}   `json:"drivingLimit"`
	RunDurationLimit             interface{}   `json:"runDurationLimit"`
	DutyTimeLimit                interface{}   `json:"dutyTimeLimit"`
	Availability                 Availability  `json:"availability"`
}

type Availability struct {
	Monday    Day         `json:"monday"`
	Tuesday   Day         `json:"tuesday"`
	Wednesday Day         `json:"wednesday"`
	Thursday  Day         `json:"thursday"`
	Friday    Day         `json:"friday"`
	Saturday  Day         `json:"saturday"`
	Sunday    Day         `json:"sunday"`
	Holiday   interface{} `json:"holiday"`
	Default   interface{} `json:"default"`
}

type Day struct {
	StartDay   string `json:"startDay"`
	StartTime  string `json:"startTime"`
	RigidStart bool   `json:"rigidStart"`
	EndDay     string `json:"endDay"`
	EndTime    string `json:"endTime"`
}

type ScheduleReponse struct {
	DriverShifts []DriverShift `json:"driverShifts"`
}

type DriverShift struct {
	DriverName       string  `json:"driverName"`
	DriverReference  string  `json:"driverReference"`
	VehicleName      string  `json:"vehicleName"`
	VehicleReference string  `json:"vehicleReference"`
	ShiftDate        string  `json:"shiftDate"`
	ShiftStartTime   string  `json:"shiftStartTime"`
	ShiftEndTime     string  `json:"shiftEndTime"`
	Runs             []Run   `json:"runs"`
	TotalWorkingTime int64   `json:"totalWorkingTime"`
	TotalDrivingTime int64   `json:"totalDrivingTime"`
	TotalDistance    float64 `json:"totalDistance"`
}

type Run struct {
	PlannedLoadingStartTime string       `json:"plannedLoadingStartTime"`
	PlannedDepartureTime    string       `json:"plannedDepartureTime"`
	PlannedReturnStartTime  string       `json:"plannedReturnStartTime"`
	PlannedCompletionTime   string       `json:"plannedCompletionTime"`
	TotalOrders             int64        `json:"totalOrders"`
	TotalDuration           int64        `json:"totalDuration"`
	TotalDistance           float64      `json:"totalDistance"`
	TotalCapacity1          float64      `json:"totalCapacity1"`
	TotalCapacity2          float64      `json:"totalCapacity2"`
	Allocations             []Allocation `json:"allocations"`
	IsLocked                bool         `json:"isLocked"`
	Reference               string       `json:"reference"`
}

type Allocation struct {
	OrderReference          string  `json:"orderReference"`
	CustomerLocationName    string  `json:"customerLocationName"`
	CustomerLocationAddress string  `json:"customerLocationAddress"`
	Latitude                float64 `json:"latitude"`
	Longitude               float64 `json:"longitude"`
	PlannedDrivingStartTime string  `json:"plannedDrivingStartTime"`
	PlannedArrivalTime      string  `json:"plannedArrivalTime"`
	PlannedCompletionTime   string  `json:"plannedCompletionTime"`
	Status                  Status  `json:"status"`
	SequenceNumber          int64   `json:"sequenceNumber"`
}

type Status string

const (
	Accepted  Status = "ACCEPTED"
	Allocated Status = "ALLOCATED"
	Completed Status = "COMPLETED"
	Driving   Status = "DRIVING"
)
