package maxoptra

func (m *Maxoptra) GetOrders() (*OrdersResponse, error) {
	result := OrdersResponse{} //make([]Order, 0)
	err := m.sendRequest("/orders", &result)
	return &result, err
}
