package maxoptra

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

type Maxoptra struct {
	apiKey     string
	subdomain  string
	HTTPClient *http.Client
	baseURL    string
}

func New(apiKey string, subdomain string) *Maxoptra {
	return &Maxoptra{
		apiKey:    apiKey,
		subdomain: subdomain,
		HTTPClient: &http.Client{
			Timeout: 5 * time.Minute,
		},
		baseURL: fmt.Sprintf("https://%s.maxoptra.com/api/v6", subdomain),
	}
}

func (m *Maxoptra) sendRequest(url string, result interface{}) (err error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", m.baseURL, url), nil)
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", m.apiKey))

	res, err := m.HTTPClient.Do(req)
	if err != nil {
		return err
	}
	defer func() {
		if tempErr := res.Body.Close(); tempErr != nil {
			err = tempErr
		}
	}()

	// Try to unmarshall into errorResponse
	if res.StatusCode != http.StatusOK {
		var errRes MaxoptraError
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return errors.New(errRes.Error)
		}
		return fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	// Unmarshall and populate result
	if err = json.NewDecoder(res.Body).Decode(&result); err != nil {
		return err
	}
	return nil
}
