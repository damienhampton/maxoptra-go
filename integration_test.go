package maxoptra_test

import (
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/damienhampton/maxoptra-go"
	"os"
	"strings"
	"testing"
)

func TestBadSubdomain(t *testing.T) {
	client := maxoptra.New("", "")
	_, err := client.GetOrders()

	if err == nil {
		t.Error("Expected error")
		return
	}
	if !strings.Contains(err.Error(), "no such host") {
		t.Errorf("Expected 'no such host' message (caused by missing subdomain), %v", err)
	}
}

func TestUnauthorized(t *testing.T) {
	client := maxoptra.New("", os.Getenv("SUBDOMAIN"))
	_, err := client.GetOrders()

	if err == nil {
		t.Error("Expected error")
		return
	}
	if err.Error() != "Internal Server Error" {
		t.Errorf("Expected 'Internal Server Error' message (caused by malformed authorization header), %v", err)
	}
}

func TestGetOrders(t *testing.T) {
	client := maxoptra.New(os.Getenv("API_KEY"), os.Getenv("SUBDOMAIN"))
	_, err := client.GetOrders()

	if err != nil {
		t.Error("Did not Expect error", err)
		return
	}
}

func setup() {
	err := godotenv.Load()
	if err != nil {
		panic(fmt.Sprintf("Could not load env vars, %v", err))
	}
}

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}
